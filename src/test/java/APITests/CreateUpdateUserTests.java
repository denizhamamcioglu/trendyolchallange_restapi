package APITests;

import Config.Constants;
import Config.RequestData;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.core.Is.is;

public class CreateUpdateUserTests {
    public RequestSpecBuilder requestBuilder;
    public RequestSpecification request;

    @BeforeTest
    public void setup() {
        requestBuilder = new RequestSpecBuilder();
        request = RestAssured.given();
        request.contentType("application/json");
    }

    @Test
    public void postNewUser_success() {
        request.body(RequestData.validUserPost);

        Response response = request.post(Constants.basePath + Constants.usersPath);
        JsonPath responseBodyJson = response.jsonPath();
        Assert.assertEquals(responseBodyJson.get("name"), "Deniz");
        Assert.assertEquals(responseBodyJson.get("job"), "QA Engineer");
        Assert.assertEquals(response.statusCode(), 201);
    }

    @Test
    public void registerNewUser_success() {
        request.body(RequestData.validRegisterPost);
        Response response = request.post(Constants.basePath + Constants.registerPath);
        JsonPath responseBodyJson = response.jsonPath();
        Assert.assertEquals(responseBodyJson.get("id"), 4);
        Assert.assertEquals(response.statusCode(), 200);
    }

    @Test
    public void registerNewUser_fail() {
        request.body(RequestData.invalidRegisterPost);
        Response response = request.post(Constants.basePath + Constants.registerPath);
        JsonPath responseBodyJson = response.jsonPath();
        Assert.assertEquals(responseBodyJson.get("error"), "Missing password");
        Assert.assertEquals(response.statusCode(), 400);
    }

    @Test
    public void updateExistingUser_success() {
        request.body(RequestData.validUpdateUserPut);
        Response response = request.put(Constants.basePath + Constants.usersPath);
        JsonPath responseBodyJson = response.jsonPath();
        Assert.assertEquals(responseBodyJson.get("job"), "Test Automation Engineer");
        Assert.assertEquals(response.statusCode(), 200);
    }
}
