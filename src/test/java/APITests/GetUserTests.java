package APITests;

import Utils.Helper;
import io.restassured.RestAssured;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class GetUserTests {
    @BeforeTest
    public static void setup() {
        Helper.setBaseURI("https://reqres.in");
        Helper.setBasePath("/api/users");
    }

    @Test(priority = 1)
    public static void getAllUsers_success() {
        RestAssured.get().then().statusCode(200);
    }

    @Test (priority = 2)
    public static void getSingleUser_success() {
        // Extracting a member id from all available member ids.
        List<Integer> memberIDs = RestAssured.get().path("data.id");

        int targetMemberID = memberIDs.get(0);

        // Getting the details of a single user.
        Helper.setBasePath("api/users/"+targetMemberID);
        RestAssured.get().then().statusCode(200);
        given().when().get().then().body("data.id", equalTo(targetMemberID));
    }

    @Test (priority = 3)
    public static void getNonExistingUser_fail() {
        Helper.setBasePath("api/users/100"); // non-existing user.
        RestAssured.get().then().statusCode(404);
    }
}

