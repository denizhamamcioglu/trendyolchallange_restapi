package Config;

public final class Constants {
    public static final String basePath = "https://reqres.in";
    public static final String usersPath = "/api/users";
    public static final String registerPath = "/api/register";
}
