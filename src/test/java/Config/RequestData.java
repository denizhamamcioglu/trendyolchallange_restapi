package Config;

public final class RequestData {
    public static final String invalidRegisterPost = "{\"email\":\"sydney@fife\"}";
    public static final String validUserPost = "{\n" +
            "    \"name\": \"Deniz\",\n" +
            "    \"job\": \"QA Engineer\"\n" +
            "}";
    public static final String validRegisterPost = "{\n" +
            "    \"email\": \"eve.holt@reqres.in\",\n" +
            "    \"password\": \"pistol\"\n" +
            "}";

    public static final String validUpdateUserPut = "{\n" +
            "    \"name\": \"Deniz\",\n" +
            "    \"job\": \"Test Automation Engineer\"\n" +
            "}";
}
