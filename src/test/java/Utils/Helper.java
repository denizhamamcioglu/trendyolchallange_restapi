package Utils;

import io.restassured.RestAssured;

public class Helper {
    public static void setBaseURI (String URI) {
        RestAssured.baseURI = URI;
    }

    public static void setBasePath (String path) {
        RestAssured.basePath = path;
    }
}
